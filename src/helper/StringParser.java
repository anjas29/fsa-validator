package helper;

import reports.Message;
import reports.Report;
import reports.Status;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class is used to:
 * 1. Match the string, to determine whether the string is matched with given format or not (using regex)
 * 2. Parse the data into ArrayString, the ArrayString will be used by FSAValidator class later
 * <p>
 * Thera are 5 state which represent 5 type of line in the INPUT File (It is better to make a constant instead of Hardcode String)
 *
 * @author Anjasmoro
 */

public class StringParser {
    public static final int STATES = 0;
    public static final int ALPHA = 1;
    public static final int INTI_ST = 2;
    public static final int FIN_ST = 3;
    public static final int TRANS = 4;

    static final String[] KEYWORDS = {"states", "alpha", "init.st", "fin.st", "trans"};

    /* Check whether the given string is match the pattern or not.
     * i.e states=[...], alpha=[...]
     */
    public static boolean matches(String st, int keywordIndex) {
        Pattern pattern = Pattern.compile(KEYWORDS[keywordIndex] + "=?\\[(.*?)\\]");
        Matcher matcher = pattern.matcher(st);
        return matcher.matches();
    }

    /* Check whether the given string is match the general pattern or not ....=[...]
     */
    public static boolean generallyMatches(String string) {
        return string.matches("([a-z.]*)=?\\[(.*?)\\]");
    }

    /* Parse the line of STATES, ALPHA, INITIAL STATE, and FINAL STATE
     * return the ArrayList of String of the STATE or ALPHA
     * */
    public static ArrayList<String> parseLine(String string, Report report) {
        ArrayList<String> arrayString = new ArrayList<>();

        // Get the string inside of bracket
        String result = Utils.splitBracket(string);

        // Check if the string is empty or not
        if (Utils.isEmpty(result)) {
            // return empty array
            return arrayString;
        } else {
            // Check if string is valid or not (only consist of letter, words, underscore, and comma)
            if (Utils.isValid(result)) {
                // Split the comma and add it into ArrayList
                for (String s : Utils.splitComma(result)) {
                    arrayString.add(s);
                }
            } else {
                // If the String is not valid then generate a report with Error Status and E5 message (Input malformed)
                report.setStatus(Status.ERROR);
                report.addMessage(new Message(Status.E5, ""));
            }
        }
        return arrayString;
    }

    public static ArrayList<String> parseTransitionLine(String string, Report report) {
        ArrayList<String> arrayString = new ArrayList<>();

        // Get the string inside of bracket
        String result = Utils.splitBracket(string);

        // Check if the string is empty or not
        if (Utils.isEmpty(result)) {
            // Return empty array
            return arrayString;
        } else {
            // Check if string is valid or not (only consist of letter, words, underscore, right sign '>', and comma)
            if (Utils.isValidTransitionCollections(result)) {
                for (String s : Utils.splitComma(result)) {
                    arrayString.add(s);
                }
            } else {
                // If the String is not valid then generate a report with Error Status and E5 message (Input malformed)
                report.setStatus(Status.ERROR);
                report.addMessage(new Message(Status.E5, ""));
            }
        }
        return arrayString;
    }

    public static ArrayList<String> parseTransition(String string) {
        ArrayList<String> transition = new ArrayList<>();
        // Check if the string has valid transition pattern or not STATE>ALPHA>STATE
        if (Utils.isValidTransition(string)) {
            // Split the right sign '>'
            for (String s : Utils.splitTransition(string)) {
                transition.add(s);
            }
            // Return 3 array [State, Alpha, State]
            return transition;
        } else {
            return null;
        }
    }
}