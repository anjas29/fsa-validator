package helper;

/**
 * This class help to check the string pattern and split bracket, comma, transition '>'
 *
 * @author Anjasmoro
 */

public class Utils {
    public static boolean isValid(String s) {
        return s != null && s.matches("^[a-zA-Z0-9_,]*$");
    }

    public static boolean isValidTransitionCollections(String s) {
        return s != null && s.matches("^[a-zA-Z0-9_,>]*$");
    }

    public static boolean isEmpty(String s) {
        return s == "";
    }

    public static String splitBracket(String s) {
        return s.substring(s.indexOf("[") + 1, s.indexOf("]"));
    }

    public static String[] splitComma(String s) {
        return s.split(",");
    }

    public static boolean isValidTransition(String s) {
        return s != null && s.matches("(.+?)>(.+?)>(.+?)");
    }

    public static String[] splitTransition(String s) {
        return s.split(">");
    }
}