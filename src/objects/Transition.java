package objects;

/**
 * This class represent Transition/Edge/ in the Graph Concept
 * It has alpha and state (next state).
 *
 * Alpha is an action, for now It is only string but I consider if it become more featured than a native string
 * It also help me to treat the code more carefully, i.e. I can't use hardcoded string
 *
 * Cost is used to calculate the path discovery.
 * I assign it 1 as default, since there is need to calculate the cost of path precisely.
 *
 * @author Anjasmoro
 */
public class Transition {
    public Alpha alpha;
    public State state;
    public int cost;

    public Transition(Alpha alpha, State state) {
        this.alpha = alpha;
        this.state = state;
        this.cost = 1;
    }
}
