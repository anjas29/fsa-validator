package objects;
/**
 * This class represent Alpha/Action.
 *
 * For now it only consist of name as a string.
 * In future use we can also to save more data, i.e. estimated memory to use this action, estimated time, etc.
 *
 * @author Anjasmoro
 */
public class Alpha {
    public String name;

    public Alpha(String name) {
        this.name = name;
    }
}