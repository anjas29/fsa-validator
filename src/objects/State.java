package objects;

import java.util.ArrayList;

/**
 * This class represent State/Node/Vertex in the Graph concept.
 * Basic identity in a State is Name and Transitions (can be empty, one, or more than one).
 * Transition consists of Alpha/Action and Next State, see Transition Class to be more detail.
 * <p>
 * minDistance and previous is used to calculate the routing and path finding. More detail in the Report
 *
 * @author Anjasmoro
 */

public class State implements Comparable<State> {
    public String name;
    public ArrayList<Transition> transitions;

    public double minDistance;
    public State previous;

    public State(String name) {
        this.name = name;
        transitions = new ArrayList<>();
        minDistance = Double.POSITIVE_INFINITY;
    }

    public Transition getTransition(int index) {
        return transitions.get(index);
    }

    public void addLink(Transition link) {
        transitions.add(link);
    }

    @Override
    public int compareTo(State s) {
        return Double.compare(minDistance, s.minDistance);
    }
}
