import helper.StringParser;

import objects.State;
import objects.Alpha;
import objects.Transition;

import reports.Message;
import reports.Report;
import reports.Status;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

/**
 * This is the main FSA Validator Class
 * -----Main Variable:
 * 1. fileName -> Input file Name to be read
 * 2. initState -> The initial State (max is 1, no need to ba array)
 * 3. stateCollection -> All of the State
 * 4. finalStateCollection -> Final State (can be more than 1), it still has dynamic link with stateCollection (not create new instance)
 * <p>
 * -----Helper Variable:
 * 1. reachStateHash: to save the list of state which is reachable, it uses hash to simplify the duplication checking
 * 2. unreachableStateCollection: to save the list of path (can be more than one state), it help to check disjoint
 * 3. parsingIndex: help to parse the line, index refers to Parsing Constant (STATES, ALPHA, TRANSITION, etc)
 * 4. Report: help to set status of FSA (complete, incomplete, error) and collect the error/warning message
 * <p>
 * -----Main Function/Method:
 * All main function is written in public
 * 1. parse: To parse The INPUT file, it use StringParser to parse the string
 * Contains Error checking related to E1, E3, E4, E5 (check the codetest description)
 * 2. fsaCheck: To check the Graph of FSA. It contains FSA Complete/Incomplete, W2, W1, W3, E2 (check the codetest description)
 * 3. generateReport: Call generate Report to generate the result string and write the result into a file (result.txt)
 * <p>
 * -----Helper/Internal Function/Method:
 * All helper function is written in private
 *
 * @author Anjasmoro
 */


public class FsaValidator {
    String fileName;
    public State initState;
    public ArrayList<State> statesCollections;
    public ArrayList<State> finalStateCollections;
    public ArrayList<Alpha> alphaCollections;

    Set<String> reachableStateHash;
    Set<Set<String>> unreachableStateCollections;
    int parsingIndex;

    public Report report;

    public FsaValidator(String fileName) {
        this.fileName = fileName;
        this.report = new Report();
        this.parsingIndex = 0;
    }

    public void parse() {
        boolean hasState = false, hasAlpha = false, hasInitialState = false, hasFinalState = false, hasTransitions = false;
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));

            String string;
            // Read string line by line
            while ((string = bufferedReader.readLine()) != null) {
                // Remove space in string
                String clearString = string.replaceAll(" ", "");
                for (int index = StringParser.STATES; index <= StringParser.TRANS; index++) {
                    // String checked based on the StringParser const
                    if (StringParser.matches(clearString, index)) {
                        switch (index) {
                            case StringParser.STATES:
                                // Parse State
                                seederState(StringParser.parseLine(clearString, report));
                                hasState = true;
                                break;
                            case StringParser.ALPHA:
                                // Parse Alpha
                                seederAlpha(StringParser.parseLine(clearString, report));
                                hasAlpha = true;
                                break;
                            case StringParser.INTI_ST:
                                // Parse Initial State
                                seedInitState(StringParser.parseLine(clearString, report));
                                hasInitialState = true;
                                break;
                            case StringParser.FIN_ST:
                                // Parse Final State
                                seedFinalState(StringParser.parseLine(clearString, report));
                                hasFinalState = true;
                                break;
                            case StringParser.TRANS:
                                // Parse Transition
                                seedTransitions(StringParser.parseTransitionLine(clearString, report));
                                hasTransitions = true;
                                break;
                        }
                    } else {
                        // Check if string is not match the pattern ...=[....]
                        if (!StringParser.generallyMatches(clearString)) {
                            // ADD E5 message
                            report.setStatus(Status.ERROR);
                            report.addMessage(new Message(Status.E5));
                        }
                    }
                }
            }
        } catch (Exception e) {
            // ADD E5 message
            report.setStatus(Status.ERROR);
            report.addMessage(new Message(Status.E5));
        }

        //Check if the file has 5 type of line
        if (!hasState || !hasAlpha || !hasInitialState || !hasFinalState || !hasTransitions) {
            // ADD E5 message in the report
            report.setStatus(Status.ERROR);
            report.addMessage(new Message(Status.E5, ""));
        }
    }

    // FSA Checking
    public void fsaCheck() {
        reachableStateHash = new HashSet<>();
        unreachableStateCollections = new HashSet<>();

        // There is no need to proceed the checking if has error
        if (report.getStatus() != Status.ERROR) {
            // Checking Acceptance State W1
            checkAcceptingState();

            // Checking Non Deterministic W3 and INCOMPLETE
            checkNonDeterministic();

            // Checking State Reachable from initial state W2
            checkPathFromInitState();

            // Checking State Disjoint E2
            // It can be done after analyzing the reachable path from initial state
            checkDisjoint();
        }
    }

    // Generate Report, mostly done in the Report Class
    public void generateReport() {
        report.generateReport();
    }

    // Checking W1 conditions
    // W1: Check whether the FSA has final state or not
    private void checkAcceptingState() {
        if (finalStateCollections.size() <= 1) {
            report.addMessage(new Message(Status.W1, ""));
        }
    }

    // Checking W3 and INCOMPLETE conditions
    // W3: Check whether a state has more than 1 link with the same ALPHA
    // INCOMPLETE: check whether all state has each alpha or not
    private void checkNonDeterministic() {
        boolean isNonDeterministic = false;

        Set<String> definedLinkAlpha = new HashSet<>();
        for (Alpha alpha : alphaCollections) {
            definedLinkAlpha.add(alpha.name);
        }

        for (State state : statesCollections) {
            Set<String> transitions = new HashSet<>();
            for (Transition transition : state.transitions) {
                if (transitions.add(transition.alpha.name) == false) {
                    isNonDeterministic = true;
                }
            }
            //Incomplete checking
            if (!definedLinkAlpha.equals(transitions)) {
                //The FSA is not complete, then set status to INCOMPLETE
                report.setStatus(Status.INCOMPLETE);
            }
        }

        if (isNonDeterministic) {
            //The FSA is non-deterministic, then add W3 message
            report.addMessage(new Message(Status.W3));
        }
    }

    /* Checking W2
     * W2: Check is there any path is not reachable from the initial state
     *
     * Using Djikstra algorithm to discover the path, initially used to check the shortest path but modified to only check the path
     * - First, discover path from INITIAL PATH. Add path which is already discovered to reachableStateHash
     * - Second, discover path which is not reachable. Try to check unreachable state to find INITIAL STATE
     * (Record any unreachable path, it will be use to check DISJOINT)
     */
    private void checkPathFromInitState() {
        boolean isUnreachable = false;
        if (initState != null) {
            // First Checking: Discover Path from initial Path
            reachableStateHash.add(initState.name);
            for (State state : statesCollections) {
                if (state != initState) {
                    if (!reachableStateHash.contains(state.name)) {
                        boolean isReachable = isReachable(initState, state);
                        //Check is there any unreachable path
                        if (!isReachable) {
                            isUnreachable = true;
                        }
                    }
                }
            }

            // Second Checking: Check from unreachable state which is not stored in reachableStateHash
            for (State state : statesCollections) {
                if (!reachableStateHash.contains(state.name)) {
                    isReachable(state, initState);
                }
            }
        }

        //ADD W2 message
        if (isUnreachable) {
            report.addMessage(new Message(Status.W2));
        }
    }

    /* Checking E2
     * E2: Check is there any disjoint path
     *
     * Compared reachable state from initial state (reachableStateHash) and unreachablePath (unreachableStateCollections)
     *
     * recursively check is there any state in UnreachableCollections that is in ReachableState.
     * if YES:
     *      - it means the paths is connected (in reverse way and can not be reached from initial state)
     *      - update the reachable state collection and do process once more
     * if NO:
     *     - stop the process
     */
    private void checkDisjoint() {
        Set<String> stateCollectionHash = new HashSet<>();
        for (State state : statesCollections) {
            stateCollectionHash.add(state.name);
        }

        boolean needToCheck = true;
        while (needToCheck) {
            boolean needToRepeat = false;
            for (Set<String> paths : unreachableStateCollections) {
                for (String s : paths) {
                    // Compare unreachable state to reachable state
                    if (reachableStateHash.contains(s)) {
                        // add the path to the reachable collection
                        addStateToReachable(paths);
                        //Delete the path from unreachable list
                        unreachableStateCollections.remove(paths);
                        needToRepeat = true;
                        break;
                    }
                }
                if (needToRepeat) {
                    break;
                }
            }
            if (needToRepeat) {
                needToCheck = true;
            } else {
                needToCheck = false;
            }
        }

        for (String state : reachableStateHash) {
            stateCollectionHash.add(state);
        }

        // Compare all the reach which is reachable and all the original state collection
        if (!reachableStateHash.equals(stateCollectionHash)) {
            report.setStatus(Status.ERROR);
            report.addMessage(new Message(Status.E2));
        }
    }

    // Check the Path Route (using Djikstra algorithm)
    // I can't explain the algorithm in here
    // The idea to check the the state is reachable from the initial state or not
    // Mark the reachable and unreachable to be used in DISJOINT Checking
    private boolean isReachable(State source, State target) {
        // Reset the distance and path (to positive infinite)
        resetMinDistance();
        source.minDistance = 0;
        source.previous = null;

        // Make queue of state to be check
        Queue<State> stateQueue = new LinkedList<State>();
        stateQueue.add(source);

        // For the E2 DISJOINT checking purpose
        Set<String> tempPath = new HashSet<>();
        if (target != initState) {
            reachableStateHash.add(source.name);
        } else {
            tempPath.add(source.name);
        }
        // - E2 DISJOINT Check

        while (!stateQueue.isEmpty()) {
            State u = stateQueue.poll();
            // Get the link of a state and try discover that state
            for (Transition e : u.transitions) {
                State v = e.state;

                // For the E2 DISJOINT checking purpose
                if (target != initState) {
                    reachableStateHash.add(v.name);
                } else {
                    tempPath.add(v.name);
                }
                // - E2 DISJOINT Check

                double weight = e.cost;
                double distance = u.minDistance + weight;
                if (distance < v.minDistance) {
                    stateQueue.remove(v);
                    v.minDistance = distance;
                    v.previous = u;
                    // add the state to be discovered next
                    stateQueue.add(v);
                }
            }
        }

        // Check if state is reachable or not
        boolean isReachable = isPreviousNull(target);

        // For the E2 DISJOINT checking purpose
        // Collect 'reachable state' and 'unreachable path'
        if (target == initState) {
            if (isReachable)
                addStateToReachable(tempPath);
            else
                unreachableStateCollections.add(tempPath);
        }
        // - E2 DISJOINT Check
        return isReachable;
    }

    // Add path to reachableStateHash
    private void addStateToReachable(Set<String> setStrings) {
        for (String s : setStrings) {
            reachableStateHash.add(s);
        }
    }

    // Convert the ArrayString to State
    private void seederState(ArrayList<String> stringCollections) {
        statesCollections = new ArrayList<>();
        for (String string : stringCollections) {
            if (!checkStateDuplication(string))
                statesCollections.add(new State(string));
        }
    }

    // Convert the ArrayString to Alpha
    private void seederAlpha(ArrayList<String> stringCollections) {
        alphaCollections = new ArrayList<>();
        for (String string : stringCollections) {
            if (!checkAlphaDuplication(string))
                alphaCollections.add(new Alpha(string));
        }
    }

    // Convert the ArrayString to Initial State
    // Check unrepresented state E1 and E4
    private void seedInitState(ArrayList<String> stringCollections) {
        //Check if init.state only one
        if (stringCollections.size() == 1 && !stringCollections.get(0).isEmpty()) {
            String initStateString = stringCollections.get(0);
            State state = findState(initStateString);
            if (state != null)
                this.initState = state;
            else {
                report.setStatus(Status.ERROR);
                report.addMessage(new Message(Status.E1, initStateString));
            }
        } else if (stringCollections.get(0).isEmpty()) {
            report.setStatus(Status.ERROR);
            report.addMessage(new Message(Status.E4));
        } else {
            report.setStatus(Status.ERROR);
            report.addMessage(new Message(Status.E5));
        }
    }

    // Convert the ArrayString to Final State
    // Check unrepresented state E1
    private void seedFinalState(ArrayList<String> stringCollections) {
        finalStateCollections = new ArrayList<>();
        for (String string : stringCollections) {
            if (!string.equals("")) {
                State state = findState(string);
                if (state != null) {
                    if (!checkFinalStateDuplication(state.name))
                        finalStateCollections.add(state);
                } else {
                    report.setStatus(Status.ERROR);
                    report.addMessage(new Message(Status.E1, string));
                }
            }
        }
    }

    // Convert the ArrayString to Transition
    // Check the string malformed E5, unrepresented state E1 and alpa E3
    private void seedTransitions(ArrayList<String> stringColletions) {
        for (String string : stringColletions) {
            ArrayList<String> transition = StringParser.parseTransition(string);
            if (transition.size() == 3) {
                String prevString = transition.get(0);
                String alphaString = transition.get(1);
                String nextString = transition.get(2);

                State prevState = findState(prevString);
                Alpha alpha = findAlpha(alphaString);
                State nextState = findState(nextString);

                if (prevState != null) {
                    if (nextState != null) {
                        if (alpha != null) {
                            Transition link = new Transition(alpha, nextState);
                            prevState.addLink(link);
                        } else {
                            report.setStatus(Status.ERROR);
                            report.addMessage(new Message(Status.E3, alphaString));
                        }
                    } else {
                        report.setStatus(Status.ERROR);
                        report.addMessage(new Message(Status.E1, nextString));
                    }
                } else {
                    report.setStatus(Status.ERROR);
                    report.addMessage(new Message(Status.E1, prevString));
                }
            } else {
                report.setStatus(Status.ERROR);
                report.addMessage(new Message(Status.E5));
            }
        }
    }

    // Find the state from the stateCollections
    // Use when make transition and assign initial/final state. it uses existing state instead of generate new instance
    private State findState(String stateName) {
        for (State state : statesCollections) {
            if (state.name.equals(stateName)) return state;
        }
        return null;
    }

    // Find the alpha from the alphaCollections
    // Use when make new transition, it uses existing alpha instead of generate new instance
    private Alpha findAlpha(String alphaName) {
        for (Alpha alpha : alphaCollections) {
            if (alpha.name.equals(alphaName)) return alpha;
        }
        return null;
    }

    //Check the State duplication by the name, use when trying to input new State
    private boolean checkStateDuplication(String stateName) {
        for (State state : statesCollections) {
            if (state.name.equals(stateName)) return true;
        }
        return false;
    }

    //Check the final state duplication by the name, use when trying to input new final state
    private boolean checkFinalStateDuplication(String stateName) {
        for (State state : finalStateCollections) {
            if (state.name.equals(stateName)) return true;
        }
        return false;
    }

    //Check the alpha duplication by the name, use when trying to input new Alpha
    private boolean checkAlphaDuplication(String alphaName) {
        for (Alpha alpha : alphaCollections) {
            if (alpha.name.equals(alphaName)) return true;
        }
        return false;
    }

    // Reset the distance matrix in Djikstra Path finding
    private void resetMinDistance() {
        for (State state : this.statesCollections) {
            if (state != null) {
                state.minDistance = Double.POSITIVE_INFINITY;
                state.previous = null;
            }
        }
    }

    // Check if the State is reachable, the previous means the state route to the SOURCE state,
    // No previous state means NO ROUTE or UNREACHABLE
    private boolean isPreviousNull(State destination) {
        if (destination.previous == null) return false;
        else return true;
    }
}