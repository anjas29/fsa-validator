package reports;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * This class represent a Report of FSA Validator
 * It has status (COMPLETE, INCOMPLETE, and ERROR)
 * <p>
 * A report can have empty or more than 1 ERROR or Warning Message
 * <p>
 * hasWarning is to check whether the error has warning or not
 *
 * @author Anjasmoro
 */


public class Report {
    private String status;
    private ArrayList<Message> messages;
    private boolean hasWarning;

    public Report() {
        status = Status.COMPLETE;
        messages = new ArrayList<>();
        hasWarning = false;
    }

    // Some logic how to set the STATUS, for example the status can not be set if it has INCOMPLETE or ERROR before
    public void setStatus(String status) {
        switch (status) {
            case Status.COMPLETE:
                if (this.status == null)
                    this.status = status;
                break;
            case Status.INCOMPLETE:
                if (this.status == Status.COMPLETE)
                    this.status = status;
                break;
            case Status.ERROR:
                this.status = status;
                break;
        }
    }

    public String getStatus() {
        return status;
    }

    public void addMessage(Message message) {
        messages.add(message);

        //This set a warning status if the message contains a Warning Message by checking the code
        if (message.code >= Status.W1) {
            hasWarning = true;
        }
    }

    // Generate a result.txt by compiling the status and message
    public void generateReport() {
        try {
            FileWriter myWriter = new FileWriter("result.txt");

            //Compiling the string of the report
            String reportResult = compileMessage();
            myWriter.write(reportResult);
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    // Generate the string of the report
    private String compileMessage() {
        String result = "";
        //Sorting the message by the code
        sortMessage();

        // Add the status of FSA
        switch (status) {
            case Status.ERROR:
                result += "Error:";
                break;
            case Status.INCOMPLETE:
                result += "FSA is incomplete";
                break;
            case Status.COMPLETE:
                result += "FSA is complete";
                break;
        }

        // Add the if there is any warning
        if (hasWarning && this.status != Status.ERROR) {
            result += "\nWarning:";
        }

        // Add the message, if it has ERROR messages, does not need to write the WARNING messages
        // Hash is used to check the data duplication
        Set<String> messageHash = new HashSet<>();
        for (Message message : messages) {
            if (this.status == Status.ERROR) {
                if (message.code <= Status.E5) {
                    //Check the duplication of message
                    if (messageHash.add(message.getMessage()) == false) {
                        //TODO: Duplication message
                    } else {
                        result += "\n" + message.getMessage();
                    }
                }
            } else {
                //Check the duplication of message
                if (messageHash.add(message.getMessage()) == false) {
                    //TODO: Duplication message
                } else {
                    result += "\n" + message.getMessage();
                }
            }
        }

        // Return the resultString which is ready to be printed in a file
        return result;
    }

    private void sortMessage() {
        Collections.sort(messages);
    }
}