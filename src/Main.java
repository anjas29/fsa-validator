/**
 * This is the main Class to call the FSA Validator class
 *
 * Complete Code description can be seen in Gitlab
 * https://gitlab.com/anjas29/fsa-validator/-/tree/master/src
 *
 * @author Anjasmoro
 */

public class Main {

    public static void main(String[] args) {
        System.out.println("Starting Validation");
        FsaValidator validator = new FsaValidator("fsa.txt");

        validator.parse();
        validator.fsaCheck();
        validator.generateReport();
    }
}